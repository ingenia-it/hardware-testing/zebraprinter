﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZebraPrinter.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ZebraPrinter.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;!DOCTYPE html&gt;
        ///&lt;html&gt;
        ///&lt;title&gt;Formato de impresi&amp;oacuten de Ticket&lt;/title&gt;
        ///&lt;meta name=&quot;viewport&quot; content=&quot;width=device-width, initial-scale=1&quot;&gt;
        ///&lt;link rel=&quot;stylesheet&quot; href=&quot;w3.css&quot;&gt;
        ///&lt;head&gt;
        ///&lt;style&gt;
        ///    @font-face {
        ///    font-family: &apos;MyLibreBarcode39_Regular&apos;;
        ///        src: url(&apos;file://##VAL001##/PegasusPrinter/Fuentes/LibreBarcode128-Regular.eot&apos;); /* IE9 Compat Modes */
        ///        src: url(&apos;file://##VAL001##/PegasusPrinter/Fuentes/LibreBarcode128-Regular.eot?#iefix&apos;) format(&apos;embedded-opentype&apos;), /* IE [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Formato {
            get {
                return ResourceManager.GetString("Formato", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap IMG001 {
            get {
                object obj = ResourceManager.GetObject("IMG001", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap printer {
            get {
                object obj = ResourceManager.GetObject("printer", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to /* W3.CSS 4.13 June 2019 by Jan Egil and Borge Refsnes */
        ///html{box-sizing:border-box}*,*:before,*:after{box-sizing:inherit}
        ////* Extract from normalize.css by Nicolas Gallagher and Jonathan Neal git.io/normalize */
        ///html{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{margin:0}
        ///article,aside,details,figcaption,figure,footer,header,main,menu,nav,section{display:block}summary{display:list-item}
        ///audio,canvas,progress,video{display:inline-block}progress{vertical-align:baseline}
        ///audio:not([contro [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string w3 {
            get {
                return ResourceManager.GetString("w3", resourceCulture);
            }
        }
    }
}
