﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace ZebraPrinter
{
    public partial class frmTestPrinter : Form
    {
        HTMLPrintData PrinterItems = new HTMLPrintData();

        public frmTestPrinter()
        {
            InitializeComponent();
        }

        private void frmPrint_Load(object sender, EventArgs e)
        {
            PruebaValores();
            ucPrint uc = new ucPrint(PrinterItems, true, true,
                @"C:\ZebraPrinter\HTML-Format",
                @"C:\ZebraPrinter\Printer_Test", "Print.html", "Print.html");
            uc.Dock = DockStyle.Fill;
            this.Controls.Add(uc);
            uc.Show();
        }

        private void PruebaValores()
        {
        string[] Encabezados = {
            "Holcim CR\nCédula Jurídica 3-101-xxxxxx\nTeléfono 22053001",
            "Otro Encabezado\nOtra Información",
            "Encabezado 3\nMás detalles",
};
            PrinterItems.Pié = "CONDICIONES:\n"
+ "\n1. Holcim(Costa Rica) S.A.únicamente garantiza los materiales contra defectos de fabricación; los reclamos deben hacerse en forma escrita y dentro de los 3 días siguientes a la entrega, salvo estipulación escrita indicando lo contrario."
+ "\n2. Tratándose de ventas de consumidor, la garantía se estima de acuerdo con el artículo 43 de la ley 7472."
+ "\n3. Todos los materiales transportados en vehículos que no sean contratados por Holcim (Costa Rica) S.A.viajan por cuenta, riesgo y responsabilidad del comprador."
+ "\n4. El cliente es el único responsable del uso que dé a los productos comprados a Holcim (Costa Rica) S.A., por lo que para el adecuado uso de los mismos, se debe consultar al profesional responsable de la obra (Miembro del CFIA), quien deberá indicar al cliente la forma correcta de uso y/o aplicación."
+ "\n5. Los pagos que deben efectuarse a favor de Holcim (Costa Rica) S.A.solo podrán ser válidamente cancelados a quien presente autorización expresa para recibir dinero a nombre de la empresa; por lo tanto, Holcim(Costa Rica) S.A.no se hace responsable por pagos efectuados a personas no autorizadas."
+ "\n6. El cliente puede cancelar los productos comprados a Holcim(Costa Rica) S.A.mediante depósito bancario en las siguientes cuentas corrientes del Banco Citybank: Colones: Cuenta";
        string[] Títulos = { "PRODUCTOS:", "INTERLOCUTORES LOGÍSTICOS:", "SOBRANTE 1" };
        string[] Enlaces = new string[1] { "www.holcim.cr" };
        string[] Variables = new string[1] { "" };
            string[] Valores = { "10.105.242.100", DateTime.Now.ToString("dd.MM.yyyy"), DateTime.Now.ToString("hh:mm:ss tt"), "7100820", "LATICRETE DE COSTA RICA, S.A.",
                      "53057350", "321438351", "12345678", "R105850673", "RODRIGO TREJOS SOLANO",
                "1505036", "TRANSPORTES ORTEGA Y ROJAS S.A.", "RC151390", "RS00001", "RS00002", "SOBRANTE1", "SOBRANTE2" };
            
            PrinterItems.Títulos = Títulos;
            PrinterItems.Enlaces = Enlaces;
            PrinterItems.Variables = Variables;
            PrinterItems.Valores = Valores;

            DataTable dt = new DataTable();
            dt.Clear();
            dt.Columns.Add("Correlativo");
            dt.Columns.Add("Cantidad");
            dt.Columns.Add("Descripción");

            DataRow _ravi;

            _ravi = dt.NewRow();
            _ravi["Correlativo"] = "1";
            _ravi["Cantidad"] = "600 SAC";
            _ravi["Descripción"] = "CEMENTO FUERTE RTCR479:2015 50kg PALETIZ";
            dt.Rows.Add(_ravi);

            _ravi = dt.NewRow();
            _ravi["Correlativo"] = "2";
            _ravi["Cantidad"] = "15 UN";
            _ravi["Descripción"] = "LAMINA PLASTICA PALETIZADO (CONTROL)";
            dt.Rows.Add(_ravi);

            _ravi = dt.NewRow();
            _ravi["Correlativo"] = "3";
            _ravi["Cantidad"] = "400 SAC";
            _ravi["Descripción"] = "CEMENTO FUERTE RTCR479:2015 50kg PALETIZ";
            dt.Rows.Add(_ravi);

            PrinterItems.Tablas = new DataTable[] {dt};

            Bitmap bmpSignature = ImageTrim((Bitmap)Properties.Resources.IMG001);
            Image signature = (Image)bmpSignature;
            PrinterItems.Imágenes = new Image[1] { signature };
        }

        public static Bitmap ImageTrim(Bitmap img)
        {
            //get image data
            BitmapData bd = img.LockBits(new Rectangle(Point.Empty, img.Size),
            ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            int[] rgbValues = new int[img.Height * img.Width];
            Marshal.Copy(bd.Scan0, rgbValues, 0, rgbValues.Length);
            img.UnlockBits(bd);


            #region determine bounds
            int left = bd.Width;
            int top = bd.Height;
            int right = 0;
            int bottom = 0;

            //determine top
            for (int i = 0; i < rgbValues.Length; i++)
            {
                int color = rgbValues[i] & 0xffffff;
                if (color != 0xffffff)
                {
                    int r = i / bd.Width;
                    int c = i % bd.Width;

                    if (left > c)
                    {
                        left = c;
                    }
                    if (right < c)
                    {
                        right = c;
                    }
                    bottom = r;
                    top = r;
                    break;
                }
            }

            //determine bottom
            for (int i = rgbValues.Length - 1; i >= 0; i--)
            {
                int color = rgbValues[i] & 0xffffff;
                if (color != 0xffffff)
                {
                    int r = i / bd.Width;
                    int c = i % bd.Width;

                    if (left > c)
                    {
                        left = c;
                    }
                    if (right < c)
                    {
                        right = c;
                    }
                    bottom = r;
                    break;
                }
            }

            if (bottom > top)
            {
                for (int r = top + 1; r < bottom; r++)
                {
                    //determine left
                    for (int c = 0; c < left; c++)
                    {
                        int color = rgbValues[r * bd.Width + c] & 0xffffff;
                        if (color != 0xffffff)
                        {
                            if (left > c)
                            {
                                left = c;
                                break;
                            }
                        }
                    }

                    //determine right
                    for (int c = bd.Width - 1; c > right; c--)
                    {
                        int color = rgbValues[r * bd.Width + c] & 0xffffff;
                        if (color != 0xffffff)
                        {
                            if (right < c)
                            {
                                right = c;
                                break;
                            }
                        }
                    }
                }
            }

            int width = right - left + 1;
            int height = bottom - top + 1;
            #endregion

            //copy image data
            int[] imgData = new int[width * height];
            for (int r = top; r <= bottom; r++)
            {
                Array.Copy(rgbValues, r * bd.Width + left, imgData, (r - top) * width, width);
            }

            //create new image
            Bitmap newImage = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            BitmapData nbd
                = newImage.LockBits(new Rectangle(0, 0, width, height),
                    ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
            Marshal.Copy(imgData, 0, nbd.Scan0, imgData.Length);
            newImage.UnlockBits(nbd);

            return newImage;
        }

    }
}
