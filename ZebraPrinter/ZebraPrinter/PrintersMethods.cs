﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;
using Microsoft.Win32;
using System.IO;

namespace ZebraPrinter
{
    internal static class PrintersMethods
    {
        /// <summary>
        /// Metodo que manda a llamar a la funcion de cambio de impresora + activacion del label
        /// </summary>
        /// <param name="printerName">Nombre de la impresora</param>
        public static void CambiarImpresora(string printerName)
        {
            SetDefaultPrinter(printerName);
        }

        /// <summary>
        /// Metodo para establecer una nueva impresora predeterminada en el sistema
        /// </summary>
        /// <param name="printerName">Nombre de la impresora</param>
        private static void SetDefaultPrinter(string printerName)
        {
            using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Printer"))
            {
                foreach (ManagementObject printer in searcher.Get())
                {
                    string name = printer["Name"] as string;
                    if (name == printerName)
                    {
                        printer.InvokeMethod("SetDefaultPrinter", null);
                        //MessageBox.Show($"Impresora predeterminada cambiada a {printerName}", "Cambio Exitoso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //MessageBox.Show($"La impresora {printerName} empezará a imprimir", "Imprimiendo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        break;
                    }
                }
            }
        }
    }
}
