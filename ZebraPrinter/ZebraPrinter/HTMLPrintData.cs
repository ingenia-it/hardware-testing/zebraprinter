﻿using System;
using System.Data;
using System.Drawing;

namespace ZebraPrinter
{
    /// <summary>
    /// Representa los datos necesarios para imprimir contenido en esta aplicación. 
    /// Puede incluir encabezado, pie de página, títulos, hipervínculos, variables, valores, una tabla y imágenes.
    /// </summary>
    public class HTMLPrintData
    {
        public string[] Encabezado = new string[1] { "" };
        public string Pié = "";
        public string[] Títulos = new string[1] { "" };
        public string[] Enlaces = new string[1] { "" };
        public string[] Variables = new string[1] { "" };
        public string[] Valores = new string[1] { "" };
        public DataTable[] Tablas = new DataTable[1];
        public string[] NombresTablas = new string[1] { "PrintTable" };

        public Image[] Imágenes = null;

        public HTMLPrintData()
        {
            Encabezado = null;
            Pié = null;
            Títulos = null;
            Enlaces = null;
            Variables = null;
            Valores = null;
            Tablas = null;
            Imágenes = null;
        }
    }
}
