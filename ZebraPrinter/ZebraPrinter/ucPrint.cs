﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using System.Threading;

namespace ZebraPrinter
{
    public partial class ucPrint : UserControl
    {
        HTMLPrintData PrinterItems = new HTMLPrintData();
        bool PrintAutomatically, HideControls = true;
        string formatDirectory, printDirectory, formatHTMLFileName, printHTMLFileName, PrinterName;

        /// <summary>
        /// UserControl ucPrint. Es un control que permite previsualizar una impresión de un HTML customizado
        /// con variables que se sutituyen dentro del HTML y además permite imprimir con dialogo de impresión,
        /// sin diálogo y con envío automático de la impresión.
        /// </summary>
        /// <param name="HtmlPrintData">Datos de impresión HTML.</param>
        /// <param name="printAutomatically">Indica si la impresión es automática.</param>
        /// <param name="hideControls">Indica si se ocultan los controles.</param>
        /// <param name="FormatDirectory">Directorio de formato.</param>
        /// <param name="PrintDirectory">Directorio de impresión.</param>
        /// <param name="FormatHTMLFileName">Nombre de archivo de formato HTML.</param>
        /// <param name="PrintHTMLFileName">Nombre de archivo de impresión HTML.</param>
        public ucPrint(HTMLPrintData HtmlPrintData, bool printAutomatically, bool hideControls, string FormatDirectory, string PrintDirectory, 
            string FormatHTMLFileName, string PrintHTMLFileName, string PrinterName)
        {
            PrinterItems = HtmlPrintData;
            HideControls = hideControls;
            PrintAutomatically = printAutomatically;
            formatDirectory = FormatDirectory;
            printDirectory = PrintDirectory;
            formatHTMLFileName = FormatHTMLFileName;
            printHTMLFileName = PrintHTMLFileName;
            this.PrinterName = PrinterName;
            InitializeComponent();
        }

        public ucPrint(HTMLPrintData HtmlPrintData, bool printAutomatically, bool hideControls, string FormatDirectory, string PrintDirectory,
            string FormatHTMLFileName, string PrintHTMLFileName)
        {
            PrinterItems = HtmlPrintData;
            HideControls = hideControls;
            PrintAutomatically = printAutomatically;
            formatDirectory = FormatDirectory;
            printDirectory = PrintDirectory;
            formatHTMLFileName = FormatHTMLFileName;
            printHTMLFileName = PrintHTMLFileName;
            this.PrinterName = "";
            InitializeComponent();
        }
        
        public ucPrint()
        {
            PrinterItems = null;
            HideControls = true;
            PrintAutomatically = false;
            formatDirectory = "";
            printDirectory = "";
            formatHTMLFileName = "";
            printHTMLFileName = "";
            this.PrinterName = "";
            InitializeComponent();
        }

        public void ChangePrintData(HTMLPrintData HtmlPrintData, bool printAutomatically, bool hideControls, string FormatDirectory, string PrintDirectory,
            string FormatHTMLFileName, string PrintHTMLFileName, string PrinterName)
        {
            PrinterItems = HtmlPrintData;
            HideControls = hideControls;
            PrintAutomatically = printAutomatically;
            formatDirectory = FormatDirectory;
            printDirectory = PrintDirectory;
            formatHTMLFileName = FormatHTMLFileName;
            printHTMLFileName = PrintHTMLFileName;
            this.PrinterName = PrinterName;

            if (PrinterName != "")
            {
                PrintersMethods.CambiarImpresora(PrinterName);
            }

            if (HideControls)
            {
                tableLayoutPanel1.ColumnStyles[0].Width = 0;
            }
            Crear_Archivos();

            webBrowser1.Navigate(string.Format("{0}/{1}", printDirectory, printHTMLFileName));
        }

        private void ucPrint_Load(object sender, EventArgs e)
        {
            if (PrinterName != "")
            {
                PrintersMethods.CambiarImpresora(PrinterName);
            }
            
            if (HideControls)
            {
                tableLayoutPanel1.ColumnStyles[0].Width = 0;
            }

            if (formatDirectory != "")
            {
                Crear_Archivos();
                webBrowser1.Navigate(string.Format("{0}/{1}", printDirectory, printHTMLFileName));
            }
            else
            {
                //webBrowser1.Navigate("about:blank");
            }
        }

        /// <summary>
        /// Crea archivos HTML basados en los datos proporcionados en un objeto HTMLPrintData.
        /// </summary>
        private void Crear_Archivos()
        {
            try
            {
                // Crea directorio de destino si no existe y replica el contenido en el destino
                Directory.CreateDirectory(printDirectory);
                CopyFilesRecursively(formatDirectory, printDirectory);

                // Lee HTML con el formato
                string Formatodata = File.ReadAllText(string.Format("{0}/{1}", printDirectory, formatHTMLFileName));

                // Crea archivo W3.CSS
                //string w3data = Properties.Resources.w3;
                //File.WriteAllText(string.Format("{0}/W3.css", curDir), w3data);

                // Crea archivo HTML de Formato para impresión
                //string Formatodata = Properties.Resources.Formato;
                //File.WriteAllText(string.Format("{0}/{1}", printDirectory, printHTMLFileName), Formatodata);

                // Creación de página web llena
                string FormatoLleno = LlenarHTML(PrinterItems, Formatodata);
                File.WriteAllText(string.Format("{0}/{1}", printDirectory, printHTMLFileName), FormatoLleno);
                CrearImágenes(PrinterItems.Imágenes, printDirectory);
            }
            catch (Exception ex)
            {
                
            }
        }

        /// <summary>
        /// Método para copiar archivos recursivamente desde el directorio de origen al directorio de destino.
        /// </summary>
        private static void CopyFilesRecursively(string sourcePath, string targetPath)
        {
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(dirPath.Replace(sourcePath, targetPath));
            }

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
            {
                File.Copy(newPath, newPath.Replace(sourcePath, targetPath), true);
            }
        }

        /// <summary>
        /// Crea imágenes a partir de un arreglo de imágenes y las guarda en el directorio especificado.
        /// </summary>
        private void CrearImágenes(Image[] imágenes, string curDir)
        {
            try
            {
                if (imágenes != null)
                {
                    for (int i = 0; i < imágenes.Length; i++)
                    {
                        imágenes[i].Save(string.Format("{0}/IMG" + (i + 1).ToString("000") + ".jpg", curDir),
                            System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Llena un archivo HTML con datos de un objeto HTMLPrintData.
        /// </summary>
        private string LlenarHTML(HTMLPrintData printerItems, string OriginalHTML)
        {
            string HTML_New = OriginalHTML;
            if (printerItems.Encabezado != null)
            {
                for (int i = 0; i < printerItems.Encabezado.Length && i < 999; i++)
                {
                    if (printerItems.Encabezado[i] != null)
                    {
                        string TextHTML = HttpUtility.HtmlEncode(printerItems.Encabezado[i]).Replace("\n", "\n<br>");
                        string VariableTag = "HEADER" + (i + 1).ToString("000");
                        HTML_New = HTML_New.Replace("##" + VariableTag + "##", TextHTML);
                    }
                }
            }
            if (printerItems.Pié != null)
            {
                string TextHTML = HttpUtility.HtmlEncode(printerItems.Pié).Replace("\n", "\n<br>");
                HTML_New = HTML_New.Replace("##FOOTER##", TextHTML);
            }

            if (printerItems.Títulos != null)
            {
                for (int i = 0; i < printerItems.Títulos.Length && i < 999; i++)
                {
                    if (printerItems.Títulos[i] != null)
                    {
                        string TextHTML = HttpUtility.HtmlEncode(printerItems.Títulos[i]).Replace("\n", "\n<br>");
                        string VariableTag = "TITLE" + (i + 1).ToString("000");
                        HTML_New = HTML_New.Replace("##" + VariableTag + "##", TextHTML);
                    }
                }
            }

            if (printerItems.Variables != null)
            {
                for (int i = 0; i < printerItems.Variables.Length && i < 999; i++)
                {
                    if (printerItems.Variables[i] != null)
                    {
                        string TextHTML = HttpUtility.HtmlEncode(printerItems.Variables[i]).Replace("\n", "\n<br>");
                        string VariableTag = "VAR" + (i + 1).ToString("000");
                        HTML_New = HTML_New.Replace("##" + VariableTag + "##", TextHTML);
                    }
                }
            }

            if (printerItems.Valores != null)
            {
                for (int i = 0; i < printerItems.Valores.Length && i < 999; i++)
                {
                    if (printerItems.Valores[i] != null)
                    {
                        string TextHTML = HttpUtility.HtmlEncode(printerItems.Valores[i]).Replace("\n", "\n<br>");
                        string VariableTag = "VAL" + (i + 1).ToString("000");
                        HTML_New = HTML_New.Replace("##" + VariableTag + "##", TextHTML);
                    }
                }
            }

            if (printerItems.Enlaces != null)
            {
                for (int i = 0; i < printerItems.Enlaces.Length && i < 999; i++)
                {
                    if (printerItems.Enlaces[i] != null)
                    {
                        string TextHTML = HttpUtility.HtmlEncode(printerItems.Enlaces[i]).Replace("\n", "\n<br>");
                        string VariableTag = "LINK" + (i + 1).ToString("000");
                        HTML_New = HTML_New.Replace("##" + VariableTag + "##", TextHTML);
                    }
                }
            }

            if (printerItems.Tablas != null)
            {
                if (printerItems.Tablas.Length == 1)
                {
                    string strFindTable = @"<table class=""PrintTable"">";
                    if (HTML_New.Contains(strFindTable))
                    {
                        // Si hay Tabla de datos a imprimir se obtendrá el Texto de cada fila
                        // a partir del HTML buscando la tabla por su propiedad class y con el 
                        // bracket de cierre de la misma tabla
                        int pFrom = HTML_New.IndexOf(strFindTable) + strFindTable.Length;
                        string Resto = HTML_New.Substring(pFrom);
                        int pTo = Resto.IndexOf("</table>") + pFrom;

                        String strFilaOriginal = HTML_New.Substring(pFrom, pTo - pFrom);
                        String strNuevasFilas = "";
                        // Ahora se hará un ciclo for para llenar cada fila de la tabla del HTML
                        // Con los datos de cada fila del DataTable de los argumentos.
                        for (int i = 0; i < printerItems.Tablas[0].Rows.Count; i++)
                        {
                            // Se copia la fila original para sustituir los datos
                            String strNuevaFila = strFilaOriginal;
                            for (int j = 0; j < printerItems.Tablas[0].Columns.Count; j++)
                            {
                                string TextHTML;
                                if (printerItems.Tablas[0].Columns[j].DataType != typeof(float))
                                {
                                    TextHTML = HttpUtility.HtmlEncode(printerItems.Tablas[0].Rows[i][j].ToString()).Replace("\n", "\n<br>");
                                }
                                else
                                {
                                    TextHTML = HttpUtility.HtmlEncode(((float)printerItems.Tablas[0].Rows[i][j]).ToString("0.00")).Replace("\n", "\n<br>");
                                }
                                string VariableTag = "T" + (j + 1).ToString("000");
                                strNuevaFila = strNuevaFila.Replace("##" + VariableTag + "##", TextHTML);
                            }
                            strNuevasFilas += strNuevaFila;
                        }
                        HTML_New = HTML_New.Replace(strFilaOriginal, strNuevasFilas);
                    }
                }
                else if (printerItems.Tablas.Length > 1 && printerItems.Tablas.Length == printerItems.NombresTablas.Length)
                {
                    for (int k = 0; k < printerItems.Tablas.Length; k++)
                    {
                        string strFindTable = @"<table class=""" + printerItems.NombresTablas[k] + @""">";
                        if (HTML_New.Contains(strFindTable))
                        {
                            // Si hay Tabla de datos a imprimir se obtendrá el Texto de cada fila
                            // a partir del HTML buscando la tabla por su propiedad class y con el 
                            // bracket de cierre de la misma tabla
                            int pFrom = HTML_New.IndexOf(strFindTable);
                            int pTo = HTML_New.IndexOf("</table><!--" + printerItems.NombresTablas[k] + "-->", pFrom) + "</table><!--".Length + printerItems.NombresTablas[k].Length + 5;
                            string TablaHTML = HTML_New.Substring(pFrom, pTo - pFrom);
                            string TablaHTMLNew = TablaHTML;

                            // Obtener la parte del HTML que contiene las filas de la tabla
                            int pFromFilas = TablaHTML.IndexOf("<tr>");
                            int pToFilas = TablaHTML.LastIndexOf("</tr>") + "</tr>".Length;
                            string strFilasOriginales = TablaHTML.Substring(pFromFilas, pToFilas - pFromFilas);

                            // Ahora se hará un ciclo for para llenar cada fila de la tabla del HTML
                            // Con los datos de cada fila del DataTable de los argumentos.
                            string FilasNuevas = "";
                            for (int i = 0; i < printerItems.Tablas[k].Rows.Count; i++)
                            {
                                // Obtener la fila original
                                string strFilaOriginal = GetRow(strFilasOriginales);
                                string strFilaNueva = strFilaOriginal;
                                // Sustituir los datos en la fila
                                for (int j = 0; j < printerItems.Tablas[k].Columns.Count; j++)
                                {
                                    string TextHTML;
                                    if (printerItems.Tablas[k].Rows[i][j].ToString() != "")
                                    {
                                        
                                        if (printerItems.Tablas[k].Columns[j].DataType != typeof(float))
                                        {
                                            TextHTML = HttpUtility.HtmlEncode(printerItems.Tablas[k].Rows[i][j].ToString()).Replace("\n", "\n<br>");
                                        }
                                        else
                                        {
                                            TextHTML = HttpUtility.HtmlEncode(((float)printerItems.Tablas[k].Rows[i][j]).ToString("0.00")).Replace("\n", "\n<br>");
                                        }
                                    }
                                    else
                                    {
                                        TextHTML = "--";
                                    }
                                    string VariableTag = "T" + (j + 1).ToString("000");
                                    strFilaNueva = strFilaNueva.Replace("##" + VariableTag + "##", TextHTML);
                                    
                                }
                                FilasNuevas += strFilaNueva + "\n";
                            }
                            TablaHTMLNew = TablaHTMLNew.Replace(GetRow(strFilasOriginales), FilasNuevas);
                            // Reemplazar la fila original en el HTML
                            HTML_New = HTML_New.Replace(TablaHTML, TablaHTMLNew);
                        }
                    }
                }

            }
            return HTML_New;
        }

        // Método para obtener una fila del HTML
        string GetRow(string strTable)
        {
            // Buscar el cierre de </thead>
            int pTheadEnd = strTable.IndexOf("</thead>");

            // Buscar la primera aparición de <tr> después de </thead>
            int pTrFrom = strTable.IndexOf("<tr>", pTheadEnd);

            // Encontrar el cierre de la primera fila <tr>
            int pTrTo = strTable.IndexOf("</tr>", pTrFrom);

            // Retornar la primera fila después de </thead>
            return strTable.Substring(pTrFrom, pTrTo - pTrFrom + "</tr>".Length);
        }
        /// <summary>
        /// Navega a la URL de la página HTML.
        /// </summary>
        private void btnHtmlNavigate_Click(object sender, EventArgs e)
        {
            string curDir = Directory.GetCurrentDirectory();
            webBrowser1.Navigate(string.Format("{0}/FormatoLleno.html", curDir));
        }

        /// <summary>
        /// Crea archivos HTML basados en los datos proporcionados.
        /// </summary>
        private void btnHTMLCrear_Click(object sender, EventArgs e)
        {
            Crear_Archivos();
        }

        /// <summary>
        /// Muestra un cuadro de diálogo de impresión.
        /// </summary>
        private void btnPrintDialog_Click(object sender, EventArgs e)
        {
            webBrowser1.ShowPrintPreviewDialog();
        }

        /// <summary>
        /// Realiza la impresión del contenido HTML en la impresora predeterminada.
        /// </summary>
        private void btnPrint_Click(object sender, EventArgs e)
        {
            webBrowser1.ShowPrintDialog();
        }

        /// <summary>
        /// Maneja el evento cuando se completa la carga del documento en el WebBrowser.
        /// </summary>
        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (PrintAutomatically)
            {
                webBrowser1.Print();
            }
            try
            {
                if (printDirectory != "")
                {
                    Image imgHTML = GetImageFromHtml(webBrowser1);
                    imgHTML.Save(string.Format("{0}/{1}", printDirectory, printHTMLFileName).Replace(".html", ".jpg"));

                }
            }
            catch (Exception ex)
            {

            }
            
        }

        /// <summary>
        /// Muestra una vista previa de impresión del contenido HTML en el WebBrowser.
        /// </summary>
        private void btnPrueba_Click(object sender, EventArgs e)
        {
            string curDir = Directory.GetCurrentDirectory();
            webBrowser1.Navigate(string.Format("{0}/Formato.html", curDir));
        }

        /// <summary>
        /// Obtiene una imagen del contenido HTML en el WebBrowser.
        /// </summary>
        /// <param name="webBrowser">Control WebBrowser.</param>
        /// <returns>Imagen generada a partir del contenido HTML.</returns>
        public Image GetImageFromHtml(WebBrowser webBrowser)
        {
            if (webBrowser.Document == null)
            {
                return null;
            }

            Cursor current = Cursor;
            Cursor = Cursors.WaitCursor;

            // Give time to WebBrowser control to finish rendering of document
            Thread.Sleep(200);

            try
            {

                // save old width / height
                Size originalSize = new Size(webBrowser1.Width, webBrowser1.Height);

                // Change to full scroll size
                int scrollHeight = webBrowser.Document.Body.ScrollRectangle.Height;
                int scrollWidth = webBrowser.Document.Body.ScrollRectangle.Width;

                DockStyle OriginalDockStyle = webBrowser.Dock;
                webBrowser.Dock = DockStyle.None;
                webBrowser.ScrollBarsEnabled = false;

                Bitmap image = new Bitmap(scrollWidth, scrollHeight);
                webBrowser.Size = new Size(scrollWidth, scrollHeight);

                // Draw to image
                webBrowser.DrawToBitmap(image, webBrowser.ClientRectangle);
                webBrowser.Size = originalSize;
                webBrowser.Dock = OriginalDockStyle;
                webBrowser.ScrollBarsEnabled = true;

                // Old one with bad quality:
                // image.Save(imageName, ImageFormat.Jpeg);

                return image;
            }

            catch { return null; }

            finally { Cursor = current; }
        }
    }
}
